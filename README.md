# Login Wallpapers

## Introduction

This project generates the login wallpapers in the
[terminal rooms](https://cncz.science.ru.nl/en/howto/terminalkamers).

Images in the `source` directory are used to compose the wallpapers.

Two different wallpapers are generated, a windows and a linux version. The resulting
wallpapers are published on <https://cncz.pages.science.ru.nl/wallpapers/>.
Here you can also find the the source images with fixed urls.

## Directory structure

The images in `image_lin[01-10]` are used for the linux background wallpaper. The images in `image_winXX` are 
used for the windows version.

```
source/
├── image_lin01 -> image_win01
├── image_lin02 -> image_win02
├── image_lin03
├── image_lin04 -> image_win04
├── image_lin05 -> image_win05
├── image_lin06 -> image_win06
├── image_lin07 -> image_win07
├── image_lin08
├── image_lin09 -> image_win09
├── image_lin10 -> image_win10
├── image_win01
├── image_win02
├── image_win04
├── image_win05
├── image_win06
├── image_win07
├── image_win08
│   └── cnczbanner_v2.png
├── image_win09
└── image_win10
```

Some of the `image_linXX` directories are in fact symbolic links that point
to the `image_winXX` directories.

## Banner positions

![test.png](https://cncz.pages.science.ru.nl/wallpapers/img/wallpapers/test.png)

Banner position 3 should not be used for the windows version. Windows overlays a clock on that position.

Members of this project can include banners in this repository. C&CZ reserves banner position 8 for a graphical login instruction.

## Image files

Use these guidelines for banners:
* do not use position 3 for the windows login wallpaper
* banners are automatically converted to `400x280` pixels. Use the same resolution (or at least aspect ratio) when designing your banner.
* do not use spaces or other funny characters in banner filenames

## End dates
Use the `remove-after-YYYY-MM-DD` in the banners' filename if you want your banner to be ignored after a certain date. For example:
```
partybanner-remove-after-2022-11-06.png
```

Will not be used after November 6th 2022.

## Preview

Generate the images locally by running `generate-wallpaper`. The script depends on
the `imagemagick` package.

## Pipeline frequency

The pipeline runs on every push to the repository and is also triggered once a day.


## Delays
It can take a while before you see the new banner apearing on the login screens.
On the windows terminal room PC's, the new background image is updated when booting windows.
